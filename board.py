import numpy as np
import time,random

class Piece:
    empty = 0
    king = 1
    queen = 2
    rook = 3
    bishop = 4
    knight = 5
    pawn = 6
    
    def __init__(self, name, player):
        self.name = name
        self.player = player

class Board:

    """
    sett(pos, piece)
    gett(pos)
    reset()
    moves()
    move(move)
    undo()
    """

    p1 = 0
    p2 = 1

    none = Piece(Piece.empty, -1)

    p1Victory = p1
    p2Victory = p2
    onGoing = 3
    draw = 4


    cross = [np.array(x) for x in [(0,1),(1,0),(0,-1),(-1,0)]]
    diags = [np.array(x) for x in [(1,1),(1,-1),(-1,-1),(-1,1)]]
    knight = [np.array(x) for x in [(1,2),(-1,2),(-2,1),(-2,-1),(-1,-2),(1,-2),(2,-1),(2,1)]]

    moves = {
            Piece.king : (cross + diags, 1),
            Piece.queen : (cross + diags, 8),
            Piece.knight : (knight, 1),
            Piece.bishop : (diags, 8),
            Piece.rook : (cross, 8),
            }
    

    def __init__(self):
        self.board = [self.none for i in range(8*8)]
        self.playerPieces = [set(), set()]
        self.turn = 0
        self.status = self.onGoing
        self.hist = []

    def _gett(self, pos):
        return self.board[pos[0] + pos[1] * 8]

    def _getPPieces(self, player = None):
        if player == None:
            player = self.turn
        return set() if player not in (0,1) else self.playerPieces[player]

    def _posInPPos(self, pos, player):
        return tuple(pos) in self._getPPieces(player)

    def _removePPos(self, pos, player):
        if self._posInPPos(pos, player):
            self._getPPieces(player).remove(tuple(pos))

    def _addPPos(self, pos, player):
        if not self._posInPPos(pos, player):
            self._getPPieces(player).add(tuple(pos))

    def _sett(self, pos, piece):
        if piece.name == Piece.empty:
            self._removePPos(pos,self.p1)
            self._removePPos(pos,self.p2)
        else:
            if self._posInPPos(pos, piece.player):
                raise Exception("two pieces in one spot")
            opp = self._opp(piece.player)
            if self._posInPPos(pos,opp):
                self._removePPos(pos,opp)
            self._addPPos(pos, piece.player)
        self.board[pos[0] + pos[1] * 8] = piece

    def getAllPieces(self):
        for i in (self.p1, self.p2):
            for pos in self._getPPieces(self, i):
                yield self._gett(pos)

    def reset(self):
        self._addNonPawns(0,self.p1)
        self._addPawns(1,self.p1)
        self._addPawns(6,self.p2)
        self._addNonPawns(7,self.p2)
        self.turn = self.p1

    def undo(self):
        if len(self.hist) == 0: return
        move, piece = self.hist.pop()
        self._sett(move[0], self._gett(move[1]))
        self._sett(move[1], piece)
        self.turn = self._opp(self.turn)
        self.status = self.onGoing

    def _randomMove(self):
        return random.choice([x for x in self.getAllMoves()])

    def isLive(self):
        return self.status == self.onGoing

    def moveRandom(self):
        self.move(self._randomMove())

    def move(self, move):
        #print("MOVEEE")
        #self._print()
        #print(move)
        if self._gett(move[1]).name == Piece.king:
            self.status = self.turn
            return
        self.hist.append((move, self._gett(move[1])))
        self._sett(move[1], self._gett(move[0]))
        self._sett(move[0], self.none)
        self.turn = self._opp(self.turn)
        if len([x for x in self.getAllMoves()]) == 0:
            self.status = self.draw

    def _pawnDirec(self):
        return np.array(((-1 if self.turn else 1), 0))

    def _getAllMoves(self):
        self._test()
        for pos in self._getPPieces():
            piece = self._gett(pos)
            if piece.name == Piece.pawn:
                direc = self._pawnDirec()
                move = (pos, pos + direc)
                if self._isMoveValid(move): #change eating for pawn
                    yield move
                    move2 = (pos, pos + direc * 2)
                    if (self.turn,pos[1]) in ((1,1), (0,6)) and self._isMoveValid(move2) and not self._isMoveEating(move):
                        yield move2
            else:
                for move in self._rangeMove(pos):
                    yield move

    def getAllMoves(self):
        self._test()
        for move in self._getAllMoves():
            if not self._posInBoard(move[0]):
                self._print()
                raise Exception("move {} has non-valid first position".format(move))
            if not self._posInBoard(move[1]):
                self._print()
                raise Exception("move {} has non-valid second position".format(move))
            if not self._isMoveValid(move):
                self._print()
                raise Exception("move {} has non-valid movemet".format(move))
            yield move

    def _posInBoard(self, pos):
        return np.amin(pos) >= 0 and np.amax(pos) < 8

    def _isMoveValid(self, move):
        return self._posInBoard(move[0]) and \
                self._posInBoard(move[1]) and \
                self._gett(move[0]).player != self.none.player and \
                self._gett(move[0]).player != self._gett(move[1]).player

    def _test(self):
        for i in range(2):
            for pos in self.playerPieces[i]:
                if self._gett(pos).name == self.none.name or self._gett(pos).player != i:
                    raise Exception("player {} points to {} which belongs to {}".format(i, self._gett(pos).name, self._gett(pos).player))
        for i in range(8):
            for j in range(8):
                pos = (i,j)
                if pos in self.playerPieces[0] or pos in self.playerPieces[1]:
                    continue
                if self._gett(pos).name == self.none.name:
                    continue
                print(pos, self.playerPieces)
                raise Exception("player {} points to {} which belongs to {}".format(i, self._gett(pos).name, self._gett(pos).player))


    def _print(self):
        print("Board state", time.time())
        for i in range(8):
            for j in range(8):
                print(self._gett((i,j)).name,end="")
            print()
        print("it is turn {}".format(self.turn))
        #print(self.playerPieces)
        #print(self.hist)

    def _rangeMove(self, pos):
        directions, dist = self.moves[self._gett(pos).name]
        for direc in directions:
            move = (pos, pos + direc)
            for i in range(dist):
                if not self._posInBoard(move[1]): break
                if self._isMoveValid(move): yield move
                if self._isMoveEating(move): break

    def _opp(self, player):
        return (player + 1) % 2 if player >= 0 else -1


    def _isMoveEating(self, move):
        return self._gett(move[0]).player == self._opp(self._gett(move[1]).player)

    def _addNonPawns(self, i, player):
        self._sett(np.array((i,0)), Piece(Piece.rook, player))
        self._sett(np.array((i,1)), Piece(Piece.knight, player))
        self._sett(np.array((i,2)), Piece(Piece.bishop, player))
        self._sett(np.array((i,3)), Piece(Piece.queen, player))
        self._sett(np.array((i,4)), Piece(Piece.king, player))
        self._sett(np.array((i,5)), Piece(Piece.bishop, player))
        self._sett(np.array((i,6)), Piece(Piece.knight, player))
        self._sett(np.array((i,7)), Piece(Piece.rook, player))

    def _addPawns(self, i, player):
        for j in range(8):
            self._sett(np.array((i,j)), Piece(Piece.pawn, player))


