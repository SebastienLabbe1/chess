import math,random
from board import Piece,Board

class ScoreFunc:
    values = {
            Piece.pawn : 1,
            Piece.bishop : 3,
            Piece.knight : 3,
            Piece.rook : 5,
            Piece.king : 1000,
            Piece.queen : 10
            }
    def constant(board):
        return 1

    def pieceValues(board):
        value = 0
        for piece in board.getAllPieces():
            value += self.values[piece.name] * (-1 if piece.player == Board.p1 else 1)

    def montecarlo(board, runs):
        board._test()
        moves = []
        bestScore = -1
        for move in board.getAllMoves():
            board.move(move)
            board._test()
            score = 0
            for _1 in range(runs):
                j = 0
                while board.isLive():
                    j += 1
                    board.moveRandom()
                score += board.status
                for _2 in range(j):
                    board.undo()
            if score == bestScore:
                moves.append(move)
            elif score > bestScore:
                bestScore = score
                moves = [move]
            board.undo()
        if not len(moves):
            print("status", board.status)
            print("player pieces", board.playerPieces)
            print("moves", [x for x in board.getAllMoves()])
            board._test()
            board._print()
            raise Exception("No moves available")
        return random.choice(moves)

class Brain:
    def __init__(self, scoreFunc, depth = 1):
        self.scoreFunc = scoreFunc
        self.depth = depth

    def move(self, board):
        if not board.isLive():
            return None
        return ScoreFunc.montecarlo(board, 0)

    def _move(self, board, curDepth = 1):
        moves = []
        #best = (-math.inf, None)
        for move in board.getAllMoves():
            board.move(move)
            score = self.scoreFunc(board)
            board.undo()
            moves.append((score,move))
            #if score == best[0]:
                #if random.random() > 0.5:
                    #best = (score, move)
            #if score > best[0]:
                #best = (score, move)
        nmove = moves[0][1]
        if curDepth == self.depth: return nmove
        board.move(nmove)
        move = self.move(board, curDepth+1)
        board.undo()
        return nmove
