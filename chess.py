#!/usr/bin/env python3

import numpy as np
import pygame
import random
import time
import math
from board import Board, Piece
from brain import Brain

class Game:
    imagePos = {
            Piece.king:0, Piece.queen:1, Piece.bishop:2, 
            Piece.knight:3, Piece.rook:4, Piece.pawn:5
            }
    def __init__(self, unit = 70):
        self.unit = unit
        pygame.init()
        self.screen = pygame.display.set_mode([self.unit * 8, self.unit * 8])
        self.board = Board()
        self.board.reset()
        self.pieces = pygame.image.load("pieces.png")
        self.pieces = pygame.transform.scale(self.pieces, np.array((6,2)) * self.unit)
        self.brain = Brain(lambda x : 1)

    def getRect(self, pos, size = 1):
        offset = (1-size)/2
        return np.array((pos[0]+offset, pos[1]+offset, size, size)) * self.unit

    def getPieceImage(self, piece):
        return np.array((self.imagePos[piece.name], piece.player, 1, 1)) * self.unit

    def draw(self):
        self.screen.fill((255,255,255))
        for i in range(8): 
            for j in range(8):
                if (i+j) % 2: pygame.draw.rect(self.screen, (120,120,120), self.getRect((i,j)))
        for pos in self.board.playerPieces[self.board.p1]:
            self.screen.blit(self.pieces, np.array(pos) * self.unit, self.getPieceImage(self.board._gett(pos)))
        for pos in self.board.playerPieces[self.board.p2]:
            self.screen.blit(self.pieces, np.array(pos) * self.unit, self.getPieceImage(self.board._gett(pos)))
        pygame.display.flip()

    def test(self):
        self.board._test()
        while True:
            self.draw()
            t = input("u(undo) / b(brain)")
            if t == "u":
                self.board.undo()
            else:
                move = self.brain.move(self.board)
                self.board.move(move)

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    self.quit()
            move = self.brain.move(self.board)
            self.draw()
            self.board.move(move)
            self.draw()
            time.sleep(1)

    def quit(self):
        pygame.quit()

g = Game()
g.test()


